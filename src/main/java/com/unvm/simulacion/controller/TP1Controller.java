package com.unvm.simulacion.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@Configuration
@EnableAutoConfiguration
@RestController
@RequestMapping("/TP1")
public class TP1Controller {

	@RequestMapping(value = "/cong-mult/{semilla}/{a}/{m}", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getCongMult(@PathVariable("semilla") Integer semillaInt,
			@PathVariable("a") Integer aInt, @PathVariable("m") Integer mInt) throws Exception {

		Map<String, String> salida = new HashMap<String, String>();
		Integer x;

		try {
			x = semillaInt + aInt + mInt;
		} catch (Exception e) {
			salida.put("error", e.getMessage());
			return salida;
		}
		
		salida.put("rnd", x.toString());
		return salida;
	}
	
	@RequestMapping(value = "/random", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getrandom() {
		Map<String, String> salida = new HashMap<String, String>();
		
		salida.put("rnd", Double.toString(Math.random()));
		return salida;
	}
	
	
	@RequestMapping(value = "*", method = RequestMethod.GET)
	public @ResponseBody String getFallback() {
		return "Fallback for GET Requests";
	}
	
	

}
